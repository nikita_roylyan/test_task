Test task
---------
Package manager: yarn  
Bundler: webpack  
Development stack: react, redux, redux-thunk  

INSTALLATION
------------
First install yarn on your local machine/
Then in your OS CLI run 'yarn install'

RUN SERVER
---------
    yarn run server

RUN WEBPACK BUNDLING
---------
    yarn run dev_local (dev enviroment, --watch)
    yarn run prod_local (prod enviroment, no --watch, minified)
