/* server */
const express = require('express')
const path = require('path')
const cors = require('cors')
const port = process.env.NODE_ENV || 3000
const app = express()

// serve static assets normally
app.use(express.static(__dirname + '/dist'))
app.use(cors({credentials: true, origin: true}))

// Handles all routes so you do not get a not found error
app.get('/*', function (request, response){
    response.sendFile(path.resolve(__dirname, 'dist', 'index.html'))
})

app.listen(port)
console.log("server started on port " + port)
