export function campaigns(state = [], action){
    switch(action.type){
        case 'CAMPAIGNS_FETCH_SUCCESS':
            return action.payload;
        case 'CAMPAIGN_STATUS_CHANGE_SUCCESS':
            return state;
        default:
            return state;
    }
}

export function campaignsError(state = false, action){
    switch(action.type){
        case 'CAMPAIGN_STATUS_CHANGE_ERROR':
            return action.payload;
        case 'CAMPAIGNS_FETCH_ERROR':
            return action.payload;
        default:
            return state;
    }
}

export function campaignStats(state = {}, action){
    switch(action.type){
        case 'CAMPAIGN_STATS_FETCH_SUCCESS':
            return action.payload;
        default:
            return state;
    }
}
