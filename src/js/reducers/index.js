import { combineReducers } from 'redux';

import { initialState } from '../app'

import { campaigns, campaignStats, campaignsError } from './campaignsReducers'

const rootReducer = (state, action) => {
  if (action.type === 'LOGOUT') {
    state = initialState
  }
  return appReducer(state, action)
}

const appReducer = combineReducers({
    campaigns,
    campaignStats,
    campaignsError
})

export default rootReducer;
