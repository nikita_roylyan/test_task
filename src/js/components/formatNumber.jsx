import React, {Component} from 'react';
import {render} from 'react-dom';
const format = require('format-number');

class FormatNumber extends Component {

    constructor(props) {
        super(props);
        this.state = {
            prefix: this.props.prefix,
            suffix: this.props.suffix,
            integerSeparator: this.props.integerSeparator,
            round: this.props.round,
            value: this.props.value
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            prefix: nextProps.prefix,
            suffix: nextProps.suffix,
            integerSeparator: nextProps.integerSeparator,
            round: nextProps.round,
            value: nextProps.value
        })
    }

    render() {
        return (
            format({prefix: this.state.prefix, suffix: this.state.suffix, integerSeparator: this.state.integerSeparator, round: this.state.round})(this.state.value)
        )
    }

}
FormatNumber.defaultProps = {
    value: 0,
    prefix: '',
    suffix: '',
    integerSeparator: ','
}

export default FormatNumber;
