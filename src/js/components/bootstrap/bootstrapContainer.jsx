import React, {Component} from 'react';

class BootstrapContainer extends Component {
  render() {
    return (
      <div className="container">
        {this.props.children}
      </div>
    );
  }
}

export default BootstrapContainer
