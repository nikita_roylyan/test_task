import React, {Component} from 'react';

class BootstrapRow extends Component {
  render() {
    return (
      <div className="row">
        {this.props.children}
      </div>
    );
  }
}

export default BootstrapRow
