import React, {Component} from 'react';
import {render} from 'react-dom';

import CampaignsListRowContainer from '../containers/CampaignsListRowContainer';

class CampaignsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            campaigns: this.props.campaigns
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({campaigns: nextProps.campaigns})
    }

    render() {
        return (
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th>Status</th>
                        <th>Name</th>
                        <th>Daily Budget</th>
                        <th>Total Budget</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.campaigns.map((campaign, i) => (
                        <CampaignsListRowContainer key={i} campaign={campaign}/>
                    ))}

                </tbody>
            </table>
        )
    }

}

export default CampaignsList;
