import React, {Component} from 'react';
import {render} from 'react-dom';
import FormatNumber from './formatNumber'
import * as d3 from 'd3';
import BootstrapRow from '../components/bootstrap/bootstrapRow';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

class CampaignStatsHeader extends Component {

    constructor(props) {
        super(props);
        this.state = {
            campaign: this.props.campaign,
            campaignsList: this.props.campaignsList,
            stats: this.props.stats,
            badgeClass: this.props.badgeClass,
            dropdownOpen: false
        }
    }

    goToList = () => {
        console.log('stats header historyu push /')
        this.props.goToList()
    }

    goToStats = (id) => {
        this.props.goToStats(id);
    }

    toggle = () => {
      this.setState({
        dropdownOpen: !this.state.dropdownOpen
      });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            campaign: nextProps.campaign,
            campaignsList: nextProps.campaignsList
        })

    }

    render() {
        return (
            <BootstrapRow>
                <div className="col">
                    <h2>{this.state.campaign.name}</h2>
                </div>
                <div className="col">
                    {/*}<div className="btn-group">
                        <button type="button" className="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Change Campaign
                        </button>
                        <div className="dropdown-menu">
                            <a className="dropdown-item" href="#">My first campaign</a>
                            <a className="dropdown-item" href="#">Another campaign</a>
                        </div>
                    </div>*/}
                    <Dropdown className={'btn-group'} isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                      <DropdownToggle className={'dropdown-toggle'} caret>
                        Change Campaign
                      </DropdownToggle>
                      <DropdownMenu>
                        {this.state.campaignsList.map((el)=>(
                            <a className="dropdown-item" key={el.id} onClick={(e) => {e.preventDefault(); this.toggle(); this.goToStats(el.id)}} href="#">{el.name}</a>
                        ))}
                      </DropdownMenu>
                    </Dropdown>
                    &nbsp; {/* To preserve a gap between buttons in original html file */}
                    <button type="button" className="btn btn-light" onClick={() => {this.toggle(); this.goToList()} }>
                        All campaigns
                    </button>
                </div>
            </BootstrapRow>
        )
    }

}

export default CampaignStatsHeader;
