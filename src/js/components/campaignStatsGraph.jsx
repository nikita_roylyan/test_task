import React, {Component} from 'react';
import {render} from 'react-dom';
import FormatNumber from './formatNumber'
import * as d3 from 'd3';
import BootstrapRow from '../components/bootstrap/bootstrapRow';
import { smartRounding } from '../shared/rounding'

class CampaignStatsGraph extends Component {

    constructor(props) {
        super(props);
        this.state = {
            campaign: this.props.campaign,
            stats: this.props.stats
        }
    }

    drawGraph = (data) => {
        let graphDOMElem = this.refs.graph;
        let specs = graphDOMElem.getBoundingClientRect();

        let margin = {top: 5, right: 5, bottom: 50, left: 5}
          , width = specs.width - margin.left - margin.right // Use the window's width
          , height = 358 - margin.top - margin.bottom; // Use the window's height

        // The number of datapoints
        let n = Object.keys(data).length;

        //Using impressions as main data source (y axis)
        let dataset = [];
        let min = 0;
        let max = 1;

        function getDate(date){
            let parts = date.split('-');
            return new Date(parts[0],parts[1]-1,parts[2]);
        }

        data.map((e)=>{
            dataset.push({
                "x": getDate(e['date']),
                "y": e['impressions']
            });
            e['impressions'] > max ? max = e['impressions'] : max = max;
        })
        max = smartRounding(max);
        dataset = dataset.map((d) => d)

        //setting date values for x axis
        let maxDate = getDate(data[0].date),
            minDate = getDate(data[data.length-1].date);

        // 5. X scale will use the index of our data
        let xScale = d3.scaleTime()
            .domain([minDate, maxDate]) // input
            .range([90, width]); // output

        // 6. Y scale will use the randomly generate number
        let yScale = d3.scaleLinear()
            .domain([0, max]) // input
            .range([height, 20]); // output

        // 7. d3's line generator
        let line = d3.line()
            .x(function(d) { return xScale(d.x); }) // set the x values for the line generator
            .y(function(d) { return yScale(d.y); }) // set the y values for the line generator
            .curve(d3.curveMonotoneX) // apply smoothing to the line

        // 1. Add the SVG to the page and employ #2
        d3.selectAll('#campaignStats svg').remove();
        let svg = d3.select("#campaignStats").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
          .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            //.call(d3.axisBottom(xScale));
            .call(d3.axisBottom(xScale).tickSize(height).tickFormat(d3.timeFormat("%Y-%m-%d")))
            .select('.domain').remove();

        svg.selectAll(".x.axis .tick text").attr("y", 10);

        svg.append("g")
            .attr("class", "y axis left")
            .attr("transform", "translate(40,0)")
            .call(d3.axisLeft(yScale).tickSize(40).tickValues([min,max*0.25, max*0.50, max*0.75, max]))
            .select('.domain').remove();

        svg.selectAll(".y.axis.left .tick text").attr("x", 0).attr("dx","1em")
        svg.selectAll(".y.axis.left .tick line").remove();

        svg.append("g")
            .attr("class", "y axis right")
            .attr("transform", "translate(60,0)")
            .call(d3.axisRight(yScale).tickSize(width-60).tickValues([min,max*0.25, max*0.50, max*0.75, max]))
            .select('.domain').remove();
        svg.selectAll(".y.axis.right .tick text").remove();

        // 9. Append the path, bind the data, and call the line generator
        svg.append("path")
            .datum(dataset) // 10. Binds data to the line
            .attr("class", "line") // Assign a class for styling
            .attr("d", line); // 11. Calls the line generator

        // svg.append("path")
        //     .datum(dataset) // 10. Binds data to the line
        //     .attr("class", "filled") // Assign a class for styling
        //     .attr("d", fill); // 11. Calls the line generator

        svg.selectAll(".dot")
            .data(dataset)
            .enter().append("circle") // Uses the enter().append() method
            .attr("class", "dot") // Assign a class for styling
            .attr("cx", function(d) { return xScale(d.x) })
            .attr("cy", function(d) { return yScale(d.y) })
            .attr("r", 4);

        //
        // var formatNumber = d3.format(".1f");
        // //let yAxis = d3.axisRight(yScale).tickSize(width).ticks(4);
        // let yAxis = d3.axisRight(yScale).tickSize(width).tickValues([max*0.25, max*0.50, max*0.75, max]);
        //
        // svg.append("g")
        //     //.attr("class", "y axis")
        //     //.call(d3.axisLeft(yScale).ticks(4));
        //     .call(customYAxis);
        //
        // function customYAxis(g) {
        //   g.call(yAxis);
        //   g.select(".domain").remove();
        //   g.selectAll(".tick line");
        //   g.selectAll(".tick text").attr("x", 4).attr("dy", -4);
        // }

    }

    componentDidUpdate() {
      if(this.state.stats && Object.keys(this.state.stats).length){
          this.drawGraph(this.state.stats)
      }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            campaign: nextProps.campaign,
            stats: nextProps.stats,
        })

    }

    render() {
        return (
            <BootstrapRow>
                <div className="col">
                    <div id="campaignStats" ref="graph"></div>
                </div>
            </BootstrapRow>
        )
    }

}

export default CampaignStatsGraph;
