import React, {Component} from 'react';
import {render} from 'react-dom';

class ErrorComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            message: this.props.message
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({message: nextProps.message})
    }

    render() {
        if(this.state.message){
            return (
                <div class="alert alert-danger" role="alert">
                  {this.state.message}
                </div>
            )
        }
        return null;
    }

}

export default ErrorComponent;
