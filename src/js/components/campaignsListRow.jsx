import React, {Component} from 'react';
import {render} from 'react-dom';
import FormatNumber from './formatNumber'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

class CampaignsListRow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            campaign: this.props.campaign,
            badgeClass: this.props.badgeClass,
            dropdownValue: this.props.dropdownValue,
            dropdownOpen: false
        }

    }

    goToStats = () => {
        console.log('gotostats')
        this.props.goToStats(this.state.campaign.id);
    }

    changeStatus = (id) => {
        console.log('change status', this.state.campaign.status)
        let newStatus = 'deactivate';
        if(this.state.campaign.status.toUpperCase() === 'INACTIVE'){
            newStatus = 'activate';
        }
        this.props.changeStatus(id, newStatus)
    }

    toggle = () => {
      this.setState({
        dropdownOpen: !this.state.dropdownOpen
      });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            campaign: nextProps.campaign,
            badgeClass: nextProps.badgeClass,
            dropdownValue: nextProps.dropdownValue
        })
    }

    render() {
        return (
            <tr>
                <td>
                    <span className={this.state.badgeClass}>{this.state.campaign.status}</span>
                </td>
                <td>{this.state.campaign.name}</td>
                <td><FormatNumber value={this.state.campaign.daily_budget} prefix={'$'} round={2}/></td>
                <td><FormatNumber value={this.state.campaign.total_budget} prefix={'$'} round={2}/></td>
                <td>
                    <Dropdown className={'show'} isOpen={this.state.dropdownOpen} size="sm" toggle={this.toggle}>
                      <DropdownToggle className={'dropdown-toggle'} caret>
                        Actions
                      </DropdownToggle>
                      <DropdownMenu>
                        <a className="dropdown-item" onClick={(e) => {e.preventDefault(); this.toggle(); this.changeStatus(this.state.campaign.id)}} href="#">{this.state.dropdownValue}</a>
                        <a className="dropdown-item" onClick={(e) => {e.preventDefault(); this.toggle(); this.goToStats()}} href="#">Stats</a>
                      </DropdownMenu>
                    </Dropdown>
                    {/*<div className="dropdown show">
                        <button className="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions
                        </button>

                        <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a className="dropdown-item" href="#"></a>
                            <a className="dropdown-item" href="#">
                                Stats
                            </a>
                        </div>
                    </div>*/}
                </td>
            </tr>
        )
    }

}

export default CampaignsListRow;
