import React, { Component } from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import Wrapper from './components/wrapper'
import Header from './components/header';

import CampaignsListContainer from './containers/campaignsListContainer'
import CampaignStatsContainer from './containers/campaignStatsContainer'

export const initialState = {

};

export const store = configureStore(initialState);

class App extends Component {

    render() {
        return (
            <Provider store={store}>
                <Router>
                    <Wrapper>
                        <Header />
                        <Switch>
                            <Route exact path="/" component={CampaignsListContainer} />
                            <Route path="/:id" component={CampaignStatsContainer}/>
                        </Switch>
                    </Wrapper>
                </Router>
            </Provider>
        )
    }

}
export default App
