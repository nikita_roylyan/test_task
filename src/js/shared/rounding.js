export function smartRounding(num) {
    let len = (num+'').length;
    let fac = Math.pow(10,len-1);
    return Math.ceil(num/fac)*fac;
}
