export const campaignsUrls = {
    url: 'https://5cd3f999-f49f-4e42-8b8b-173c7185f093.mock.pstmn.io',
    get: '/campaigns',
    activate: '/activate',
    deactivate: '/deactivate',
    stats: '/stats'
}

export const errorMsg = {
    campaignStatusChange: 'Error during campaign status change!',
    campaignsFetchError: 'Error while loading campaigns!'
}
