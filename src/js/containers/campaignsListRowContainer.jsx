import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { BrowserRouter as Router, withRouter } from 'react-router-dom';

import CampaignsListRow from '../components/campaignsListRow';
import { campaignStatusChangeDispatcher } from '../actions/campaignsActions'

class CampaignsListRowContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            campaign: this.props.campaign
        }

    }

    badgeClass = (status) => {
        if(status.toUpperCase() === 'ACTIVE'){
            return 'badge badge-primary';
        }else{
            return 'badge badge-secondary';
        }
    }

    dropdownValue = (status) => {
        if(status.toUpperCase() === 'ACTIVE'){
            return 'Deactivate';
        }else{
            return 'Activate';
        }
    }

    goToStats = (id) => {
        this.props.history.push('/'+id);
    }

    campaignStatusChange = (id, action) => {
        this.props.actions.campaignStatusChange(id,action);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            campaign: nextProps.campaign
        })
    }

    render() {
        return (
            <CampaignsListRow changeStatus={this.campaignStatusChange} goToStats={this.goToStats} badgeClass={this.badgeClass(this.state.campaign.status)} dropdownValue={this.dropdownValue(this.state.campaign.status)} campaign={this.state.campaign}/>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        campaigns: state.campaigns
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
      actions: {
          campaignStatusChange: (id,action) => dispatch(campaignStatusChangeDispatcher(id, action))
      }
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CampaignsListRowContainer));
