import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { BrowserRouter as Router, withRouter } from 'react-router-dom';

import BootstrapContainer from '../components/bootstrap/bootstrapContainer';
import BootstrapRow from '../components/bootstrap/bootstrapRow';
import CampaignStatsHeader from '../components/campaignStatsHeader';
import CampaignStatsGraph from '../components/campaignStatsGraph';

import { getCampaignsDispatcher } from '../actions/campaignsActions'

import { campaignsUrls } from '../shared/constants'

class CampaignStatsContainer extends Component {

    constructor(props){
        super(props);
        this.state = {
            campaign: {},
            stats: {}
        }
    }

    getCampaignStats = (id) => {
        let self = this;
        let url = campaignsUrls.url + campaignsUrls.get + '/' + id + campaignsUrls.stats;
        fetch(url, {method: 'GET', cache: 'reload'})
            .then((response) => response.json())
            .then((campaignStats) => {
                self.setState({stats: campaignStats})
            })
            .catch((err) => {
                console.log(err)
            })
    }

    goToList = () => {
        this.props.history.push('/')
    }

    goToStats = (id) => {
        this.props.history.push('/'+id)
    }

    componentDidMount(){
        let self = this;
        if(this.props.campaigns && this.props.campaigns.length > 0){
            let id = this.props.match.params.id;
            let index = this.props.campaigns.findIndex((obj => obj.id == id));
            if(index > -1){
                self.setState({campaign: this.props.campaigns[index]})
                self.getCampaignStats(id);
            }else{
                //no stats or id for this campaign
            }

        }else{
            this.props.actions.getCampaigns()
        }
    }

    componentDidUpdate(prevProps, prevState){
        let self = this;

        //if we had to GET campaigns list here
        if(this.props.campaigns.length > 0 && prevProps.campaigns.length === 0){
            let id = this.props.match.params.id;
            let index = this.props.campaigns.findIndex((obj => obj.id == id));
            if(index > -1){
                self.setState({campaign: this.props.campaigns[index]})
                self.getCampaignStats(id);
            }
        }

        //if we clicked new campaign
        if(this.props.match.params.id !== prevProps.match.params.id){
            let id = this.props.match.params.id;
            let index = this.props.campaigns.findIndex((obj => obj.id == id));
            if(index > -1){
                self.setState({campaign: this.props.campaigns[index]})
                self.getCampaignStats(id);
            }
        }
    }

    render() {
        return (
            <BootstrapContainer>
                <CampaignStatsHeader campaignsList={this.props.campaigns} campaign={this.state.campaign} goToList={this.goToList} goToStats={this.goToStats}/>
                <CampaignStatsGraph campaign={this.state.campaign} stats={this.state.stats}/>
            </BootstrapContainer>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        campaigns: state.campaigns
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
      actions: {
          getCampaigns: () => dispatch(getCampaignsDispatcher())
      }
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CampaignStatsContainer));
