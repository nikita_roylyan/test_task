import React, { Component } from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Switch, Route, Link, withRouter } from 'react-router-dom';

import BootstrapContainer from '../components/bootstrap/bootstrapContainer';
import CampaignsList from '../components/campaignsList';

import ErrorComponent from '../components/errorComponent';

import { getCampaignsDispatcher } from '../actions/campaignsActions'

class CampaignsListContainer extends Component {

    constructor(props){
        super(props);

    }

    componentDidMount(){
        this.props.actions.getCampaigns();
    }

    render() {
        return (
            <BootstrapContainer>
                <ErrorComponent message={this.props.campaignsError}/>
                <CampaignsList campaigns={this.props.campaigns}/>
            </BootstrapContainer>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        campaigns: state.campaigns,
        campaignsError: state.campaignsError
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
      actions: {
          getCampaigns: () => dispatch(getCampaignsDispatcher())
      }
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CampaignsListContainer));
