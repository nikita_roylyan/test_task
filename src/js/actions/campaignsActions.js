import { campaignsUrls, errorMsg } from '../shared/constants';


//GET campaigns (list)
export function getCampaignsDispatcher(){
    return (dispatch) => {
        let url = campaignsUrls.url + campaignsUrls.get;
        fetch(url, {method: 'GET', cache: 'reload'})
            .then((response) => response.json())
            .then((campaigns) => {
                dispatch(getCampaignsAction('CAMPAIGNS_FETCH_SUCCESS',campaigns))
                dispatch(getCampaignsAction('CAMPAIGNS_FETCH_ERROR',false))
            })
            .catch((campaigns) => {
                dispatch(getCampaignsAction('CAMPAIGNS_FETCH_ERROR',errorMsg.campaignsFetchError))
            })
    }
}

export function getCampaignsAction(type,payload){
    return {
        type: type,
        payload: payload
    };
}

//POST activate/deactivate campaign
export function campaignStatusChangeDispatcher(id, action){
    return (dispatch) => {

        let url = campaignsUrls.url + campaignsUrls.get+'/'+id+campaignsUrls[action];
        fetch(url, {
            method: 'post'
        })
        .then((response) => {
            if(response.status === 200){
                return response.json()
            }else{
                dispatch(campaignStatusChangeAction('CAMPAIGN_STATUS_CHANGE_ERROR',errorMsg.campaignStatusChange))
            }
        })
        .then((res) => {
            dispatch(campaignStatusChangeAction('CAMPAIGN_STATUS_CHANGE_SUCCESS',res))
            dispatch(campaignStatusChangeAction('CAMPAIGN_STATUS_CHANGE_ERROR',false))
        })
        .catch((err) => {
            dispatch(campaignStatusChangeAction('CAMPAIGN_STATUS_CHANGE_ERROR',errorMsg.campaignStatusChange))
        })
    }
}


export function campaignStatusChangeAction(type,payload){
    return {
        type: type,
        payload: payload
    };
}
