/* css */
// const reqCss = require.context('./css', true, /\.css$/);
// reqCss.keys().forEach(function (key) {
//     reqCss(key);
// });

/* sass */
const reqSass = require.context('./sass', true, /\.sass$/);
reqSass.keys().forEach(function (key) {
    reqSass(key);
});
  
/* Js */
const entry = require('./js/index.js')

/*imgs*/
// const reqImgs = require.context('./images', true, /\.(jpg|jpeg|png|svg)$/);
// reqImgs.keys().forEach(function (key) {
//     reqImgs(key);
// });
