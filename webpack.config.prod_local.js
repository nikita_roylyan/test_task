const webpack = require('webpack');
const merge = require('webpack-merge');

const main = require('./webpack.config.main.js');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCSS = new ExtractTextPlugin('css/main.css');
const indexHTML = new HtmlWebpackPlugin({title: 'Express Coin React', template: './src/index.html', filename: 'index.html', hash: true});

const Uglify = require("uglifyjs-webpack-plugin");

module.exports = merge(main, {

    module: {
        rules: [
            {
                test: /\.scss|\.sass|\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {minimize: true}
                        },
                        {
                            loader: 'sass-loader',
                            options: {minimize: true}
                        }
                    ]
                    })﻿
            }
        ]
    },
    plugins: [
        extractCSS,
        new Uglify({
          uglifyOptions: {
            compress: {
              warnings: false,
            },
            output: {
              comments: false
            }
          },
          sourceMap: true
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        })
    ],
});
