const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractCSS = new ExtractTextPlugin('css/main.css');
const indexHTML = new HtmlWebpackPlugin({title: 'Express Coin React', template: './src/index.html', filename: 'index.html', hash: true});

module.exports = {
    entry: {
        index: './src/index.js'
    },
    output: {
        path: __dirname + '/dist/',
        filename: 'bundle.js',
        //publicPath: '/static/' was used for webpack dev devServer
    },
    resolve: {
      extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        loader: 'babel-loader',
                        query: {
                            babelrc: false,
                            presets: ['env', 'react'],
                            plugins: ['transform-class-properties']
                        }
                    },
                ]
            },
            {
                test: /\.(jpg|jpeg|png|svg|eot|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options:{
                            name: '[path][name].[ext]',
                            context: __dirname+'/src/',
                        }
                    }
                ]
            },
        ]
    },
    plugins: [
        indexHTML
    ],
}
